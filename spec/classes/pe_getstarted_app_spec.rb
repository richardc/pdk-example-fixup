require 'spec_helper'

describe 'pe_getstarted_app', :type => :class do
  let(:facts) do
    {
      :operatingsystemrelease => "14.04",
      :osfamily => "Debian",
      :operatingsystem => "Ubuntu",
      :lsbdistrelease => "Trusty",
    }
  end

  describe "stardard content" do
    it { is_expected.to contain_class('apache').with('mpm_module' => 'prefork') }

    it {
      is_expected.to contain_apache__vhost('pe_getstarted_app').with(
        'port' => '80',
      )
    }

    it {
      is_expected.to contain_file("/var/www/pe_getstarted_app/index.php").with(
        'ensure'  => 'file',
        'content' => "<?php phpinfo() ?>\n",
        'mode' => '0644',
      )
    }
  end

  describe "custom content" do
    let(:params) do
      { 'content' => "custom\n" }
    end

    it {
      is_expected.to contain_file("/var/www/pe_getstarted_app/index.php").with(
        'ensure'  => 'file',
        'content' => "custom\n",
        'mode' => '0644',
      )
    }
  end
end
